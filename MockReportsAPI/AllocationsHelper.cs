﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using MockReportsAPI.Models;

namespace MockReportsAPI
{
    public static class AllocationsHelper
    {
        public enum CategoryTypes
        {
            AssetCategory = 0,
            SourceOfReturn = 1,
            RiskCategory = 2
        }

        public static decimal? WeightAverage(this List<AssetClass> records, Func<AssetClass, decimal?> value, string assetCategoryName)
        {
            var filteredRecords = records.AsQueryable().Where(CategoriesFilter(assetCategoryName));
            var weightedValueSum = filteredRecords.Sum(x => value(x) * x.Percentage);
            var weightSum = filteredRecords.Sum(x => x.Percentage);

            if (weightSum != 0)
                return weightedValueSum / weightSum;
            else
                throw new DivideByZeroException();
        }

        public static Expression<Func<AssetClass, bool>> CategoriesFilter(string assetCategoryName)
        {
            return (x => x.AssetCategoryName == assetCategoryName);
        }
    }
}
