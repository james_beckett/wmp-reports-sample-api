﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using MockReportsAPI.Models;
using Newtonsoft.Json;

namespace MockReportsAPI.Services
{
    public class AllocationsService : IAllocationService
    {
        public List<AssetClassDetail> GetAssetClassDetails(string file, List<string> accountNos = null)
        {
            var details = JsonConvert.DeserializeObject<List<AssetClassDetail>>(File.ReadAllText(file));
            if (accountNos != null)
            {
                details = details.Where(x => accountNos.Contains(x.AccountNo)).ToList();
            }

            return details;
        }

        public List<T> GetLookup<T>(string file)
        {
            var lookups = JsonConvert.DeserializeObject<List<T>>(File.ReadAllText(file));
            return lookups;
        }

        public List<AssetClass> CalculateAssetClasses(string file, string assetClassLookupFile, int categoryType, List<string> accountNos = null)
        {
            var details = GetAssetClassDetails(file, accountNos);
            var assetClasslookup = GetLookup<AssetClassLookup>(assetClassLookupFile);

            var sum = details.Sum(x => x.DollarValue);

            var assetClasses =
                details.GroupBy(x => new
                    {
                        x.AssetClassName,
                        Category = categoryType == 2 ? x.RiskCategoryName : (categoryType == 1 ? x.SourceOfReturnName: x.AssetCategoryName)
                    })
                    .Select(a => new AssetClass
                    {
                        AssetClassName = a.Key.AssetClassName,
                        AssetCategoryName = a.Key.Category,
                        DollarValue = a.Sum(b => b.DollarValue),
                        Percentage = a.Sum(b => b.DollarValue) / sum
                    });

            var result = assetClasses.Select(x => new AssetClass
            {
                AssetClassName = x.AssetClassName,
                AssetCategoryName = x.AssetCategoryName,
                DollarValue = x.DollarValue,
                Percentage = x.Percentage,
                Risk = assetClasslookup.FirstOrDefault(y => y.AssetClassName == x.AssetClassName && y.AssetCategoryName == x.AssetCategoryName)?.Risk,
                Return = assetClasslookup.FirstOrDefault(y => y.AssetClassName == x.AssetClassName && y.AssetCategoryName == x.AssetCategoryName)?.Return,
                Yield = assetClasslookup.FirstOrDefault(y => y.AssetClassName == x.AssetClassName && y.AssetCategoryName == x.AssetCategoryName)?.Yield,
                ColorCode = assetClasslookup.FirstOrDefault(y => y.AssetClassName == x.AssetClassName && y.AssetCategoryName == x.AssetCategoryName)?.ColorCode
            });

            return result.ToList();
        }

        public List<AssetCategoryType> CalculateAssetCategoryTypes(string file, string assetCategoryLookupFile, string assetClassLookupFile, int categoryType, List<string> accountNos = null)
        {
            var assetClasses = CalculateAssetClasses(file, assetClassLookupFile, categoryType, accountNos);
            var assetCategoryLookup = GetLookup<AssetCategoryLookup>(assetCategoryLookupFile);

            var sum = assetClasses.Sum(x => x.DollarValue);

            var assetCategories =
                assetClasses.GroupBy(x => new {x.AssetCategoryName})
                    .Select(a => new AssetCategoryType
                    {
                        AssetCategoryName = a.Key.AssetCategoryName,
                        DollarValue = a.Sum(b => b.DollarValue),
                        Percentage = a.Sum(b => b.DollarValue) / sum
                    });

            var result = assetCategories.Select(x => new AssetCategoryType
            {
                AssetCategoryName = x.AssetCategoryName,
                DollarValue = x.DollarValue,
                Percentage = x.Percentage,
                Return = assetClasses.WeightAverage(y => y.Return, x.AssetCategoryName),
                Risk = assetClasses.WeightAverage(y => y.Risk, x.AssetCategoryName),
                Yield = assetClasses.WeightAverage(y => y.Yield, x.AssetCategoryName),
                ColorCode = assetCategoryLookup.FirstOrDefault(y => y.AssetCategoryName == x.AssetCategoryName)?.ColorCode
            });

            return result.ToList();
        }
    }
}
