﻿using MockReportsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MockReportsAPI.Services
{
    public interface IAllocationService
    {
        List<AssetClassDetail> GetAssetClassDetails(string file, List<string> accountNos = null);
        List<T> GetLookup<T>(string file);
        List<AssetClass> CalculateAssetClasses(string file, string assetClassLookupFile, int categoryType, List<string> accountNos = null);
        List<AssetCategoryType> CalculateAssetCategoryTypes(string file, string assetCategoryLookupFile, string assetClassLookupFile, int categoryType, List<string> accountNos = null);
    }
}
