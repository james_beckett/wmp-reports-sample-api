﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MockReportsAPI.Models
{
    public class AssetClassLookup
    {
        [JsonProperty(PropertyName = "AssetClass")]
        public string AssetClassName { get; set; }

        [JsonProperty(PropertyName = "Category")]
        public string AssetCategoryName { get; set; }
        public decimal? Risk { get; set; }
        public decimal? Return { get; set; }
        public decimal? Yield { get; set; }
        public string ColorCode { get; set; }
    }
}
