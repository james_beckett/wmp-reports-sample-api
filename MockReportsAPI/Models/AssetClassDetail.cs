﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MockReportsAPI.Models
{
    public class AssetClassDetail
    {
        public int ScenarioId { get; set; }
        public string PortfolioTypeCode { get; set; }
        public string AssetClassName { get; set; }
        [JsonProperty(PropertyName = "AssetCategory")]
        public string AssetCategoryName { get; set; }
        [JsonProperty(PropertyName = "SourceOfReturn")]
        public string SourceOfReturnName { get; set; }
        [JsonProperty(PropertyName = "RiskCategory")]
        public string RiskCategoryName { get; set; }
        public string AccountNo { get; set; }
        public string TaxStatus { get; set; }
        public decimal DollarValue { get; set; }
        [JsonProperty(PropertyName = "DollarPercent")]
        public decimal Percentage { get; set; }
    }
}
