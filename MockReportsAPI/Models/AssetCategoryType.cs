﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MockReportsAPI.Models
{
    public class AssetCategoryType
    {
        public string AssetCategoryName { get; set; }
        public decimal DollarValue { get; set; }
        public decimal Percentage { get; set; }
        public decimal? Risk { get; set; }
        public decimal? Return { get; set; }
        public decimal? Yield { get; set; }
        public string ColorCode { get; set; }
    }
}
