﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MockReportsAPI.Models
{
    public class AssetCategoryLookup
    {
        [JsonProperty(PropertyName = "Category")]
        public string AssetCategoryName { get; set; }

        public string ColorCode { get; set; }
    }
}
