﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MockReportsAPI
{
    public static class ReportsConstants
    {
        public static string FileDetails = @"data\details.json";
        public static string FileAssetClassByAssetCategoryLookup = @"data\AssetCategoryLookup_AssetClass.json";
        public static string FileAssetClassBySourceOfReturnLookup = @"data\SourceOfReturnLookup_AssetClass.json";
        public static string FileAssetClassByRiskCategoryLookup = @"data\RiskCategoryLookup_AssetClass.json";
        public static string FileAssetCategoryLookup = @"data\AssetCategoryLookup.json";
        public static string FileSourceOfReturnCategoryLookup = @"data\SourceOfReturnLookup.json";
        public static string FileRiskCategoryLookup = @"data\RiskCategoryLookup.json";
    }
}
