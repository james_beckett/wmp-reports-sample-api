﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MockReportsAPI.Services;

namespace MockReportsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IAllocationService _allocationService;

        public ReportsController(IAllocationService allocationService)
        {
            _allocationService = allocationService;
        }

        // GET: api/Reports
        [HttpGet("details")]
        public IActionResult Get()
        {
            List<string> accountNos = new List<string>() { "3TN-000001", "3TN-000002" };
            var details = _allocationService.GetAssetClassDetails(ReportsConstants.FileDetails);
            return Ok(details);


        }

        [HttpGet("assetclasses")]
        public IActionResult GetAssetClasses()
        {
            List<string> accountNos = new List<string>() { "3TN-000001", "3TN-000002" };
            var assetClasses = _allocationService.CalculateAssetClasses(ReportsConstants.FileDetails, ReportsConstants.FileAssetClassByRiskCategoryLookup, (int)AllocationsHelper.CategoryTypes.RiskCategory);
            return Ok(assetClasses);
        }

        // POST: api/Reports
        [HttpGet("assetcategories")]
        public IActionResult GetAssetCategories()
        {
            List<string> accountNos = new List<string>() { "3TN-000001", "3TN-000002" };
            var assetCategories = _allocationService.CalculateAssetCategoryTypes(
                ReportsConstants.FileDetails, 
                ReportsConstants.FileRiskCategoryLookup,
                ReportsConstants.FileAssetClassByRiskCategoryLookup,
                (int)AllocationsHelper.CategoryTypes.RiskCategory);

            return Ok(assetCategories);
        }
    }
}
